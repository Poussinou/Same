Same - It's a simple Game.

All fields must have the same color.
Press on a field to change it's color and that of the neighbouring fields.

Also in three-color mode. 

The colours displayed can be changed randomly.
A long press on the color palette resets the colors to their original color.
