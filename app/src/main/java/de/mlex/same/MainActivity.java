package de.mlex.same;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.snackbar.Snackbar;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnA1, btnA2, btnA3, btnB1, btnB2, btnB3, btnC1, btnC2, btnC3;
    private int[] same9 = new int[9];

    private TextView txtBestAttempt;

    private ConstraintLayout parent;

    private int counter = 0, bestAttempt2, worstAttempt2, average2 = 0, bestAttempt3, worstAttempt3, average3 = 0, numberGames2 = 0, numberGames3 = 0, numberCounter2, numberCounter3, depthM = 2;

    private String color1;
    private String color2;
    private String color3;
    private String StrOldAverage2;
    private String StrOldAverage3;

    private boolean numberOfColors = false; // false = two; true = three;
    private boolean runGame = true, clickedFirstTime = true;
    private ImageView imgNumberColors, imgColors;

    @Override
    public void onClick(View view) {
        if ((view.getId() == R.id.btnA1) && runGame) {
            counter++;
            changeColor(0, 1, 3);
            loadCorrectColor();
            checkWin9();
        } else if (view.getId() == R.id.btnA2 && runGame) {
            counter++;
            changeColor(0, 1, 2, 4);
            loadCorrectColor();
            checkWin9();
        } else if (view.getId() == R.id.btnA3 && runGame) {
            counter++;
            changeColor(1, 2, 5);
            loadCorrectColor();
            checkWin9();
        }
        else if (view.getId() == R.id.btnB1 && runGame) {
            counter++;
            changeColor(0, 3, 4, 6);
            loadCorrectColor();
            checkWin9();
        }
        else if (view.getId() == R.id.btnB2 && runGame) {
            counter++;
            changeColor(1, 3, 4, 5, 7);
            loadCorrectColor();
            checkWin9();
        }
        else if (view.getId() == R.id.btnB3 && runGame) {
            counter++;
            changeColor(2, 5, 4, 8);
            loadCorrectColor();
            checkWin9();
        }
        else if (view.getId() == R.id.btnC1 && runGame) {
            counter++;
            changeColor(3, 6, 7);
            loadCorrectColor();
            checkWin9();
        }
        else if (view.getId() == R.id.btnC2 && runGame) {
            counter++;
            changeColor(4, 6, 7, 8);
            loadCorrectColor();
            checkWin9();
        }
        else if (view.getId() == R.id.btnC3 && runGame) {
            counter++;
            changeColor(5, 7, 8);
            loadCorrectColor();
            checkWin9();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        parent = findViewById(R.id.parent);
        ConstraintLayout constLayout9 = findViewById(R.id.constLayout9);
        txtBestAttempt = findViewById(R.id.txtBestAttempt);

        constLayout9.setVisibility(View.VISIBLE);

        initStuff();
        randomizeGame();

        Toast.makeText(this, "Here we go! Press on a field.", Toast.LENGTH_LONG).show();

        imgColors.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                color1 = "#b3b3ff";
                color2 = "#131339";
                color3 = "#4780b8";
                loadCorrectColor();
                return true;
            }
        });

    }

    public void onImgNumberColorsClicked(View view) {
        if (numberOfColors) {
            imgNumberColors.setImageResource(R.drawable.ic_3colors);
            if (average2 == 0) {
                txtBestAttempt.setText("best attempt: -\naverage: - (-) \nworst attempt: -");

            } else {
                txtBestAttempt.setText("best attempt: " + bestAttempt2
                        + "\naverage: " + average2 + " (" + StrOldAverage2 + ")"
                        + "\nworst attempt: " + worstAttempt2);
            }
        } else {
            imgNumberColors.setImageResource(R.drawable.ic_2colors);
            if (average3 == 0) {
                txtBestAttempt.setText("best attempt: -\naverage: - (-) \nworst attempt: -");

            } else {
                txtBestAttempt.setText("best attempt: " + bestAttempt3
                        + "\naverage: " + average3 + " (" + StrOldAverage3 + ")"
                        + "\nworst attempt: " + worstAttempt3);
            }
        }
        numberOfColors = !numberOfColors;
        randomizeGame();
    }

    public void onImgColorsClicked(View view) {
        if (clickedFirstTime) {
            Toast.makeText(this, "You can reset the colors by long-pressing the button.", Toast.LENGTH_SHORT).show();
            clickedFirstTime = false;
        }
        
        Random rand = new Random();
        int red = rand.nextInt(130) + 124;
        int green = rand.nextInt(130) + 124;
        int blue = rand.nextInt(130) + 124;
        color1 = "#" + String.format("%02x%02x%02x", red, green, blue);
        red = rand.nextInt(130);
        green = rand.nextInt(130);
        blue = rand.nextInt(130);
        color2 = "#" + String.format("%02x%02x%02x", red, green, blue);
        if (numberOfColors) {
            red = rand.nextInt(254);
            green = rand.nextInt(254);
            blue = rand.nextInt(254);
            color3 = "#" + String.format("%02x%02x%02x", red, green, blue);
        }
        loadCorrectColor();
    }

    public void changeColor(int a, int b, int c) {
        if (!numberOfColors) {
            if (same9[a] == 0) {
                same9[a] = 1;
            } else {
                same9[a] = 0;
            }
            if (same9[b] == 0) {
                same9[b] = 1;
            } else {
                same9[b] = 0;
            }
            if (same9[c] == 0) {
                same9[c] = 1;
            } else {
                same9[c] = 0;
            }
        } else {
            if (same9[a] == 0) {
                same9[a] = 1;
            } else if (same9[a] == 1) {
                same9[a] = 20;
            } else {
                same9[a] = 0;
            }
            if (same9[b] == 0) {
                same9[b] = 1;
            } else if (same9[b] == 1) {
                same9[b] = 20;
            } else {
                same9[b] = 0;
            }
            if (same9[c] == 0) {
                same9[c] = 1;
            } else if (same9[c] == 1) {
                same9[c] = 20;
            } else {
                same9[c] = 0;
            }
        }
    }

    public void changeColor(int a, int b, int c, int d) {
        if (!numberOfColors) {
            if (same9[a] == 0) {
                same9[a] = 1;
            } else {
                same9[a] = 0;
            }
            if (same9[b] == 0) {
                same9[b] = 1;
            } else {
                same9[b] = 0;
            }
            if (same9[c] == 0) {
                same9[c] = 1;
            } else {
                same9[c] = 0;
            }
            if (same9[d] == 0) {
                same9[d] = 1;
            } else {
                same9[d] = 0;
            }
        } else {
            if (same9[a] == 0) {
                same9[a] = 1;
            } else if (same9[a] == 1) {
                same9[a] = 20;
            } else {
                same9[a] = 0;
            }
            if (same9[b] == 0) {
                same9[b] = 1;
            } else if (same9[b] == 1) {
                same9[b] = 20;
            } else {
                same9[b] = 0;
            }
            if (same9[c] == 0) {
                same9[c] = 1;
            } else if (same9[c] == 1) {
                same9[c] = 20;
            } else {
                same9[c] = 0;
            }
            if (same9[d] == 0) {
                same9[d] = 1;
            } else if (same9[d] == 1) {
                same9[d] = 20;
            } else {
                same9[d] = 0;
            }
        }
    }

    public void changeColor(int a, int b, int c, int d, int e) {
        if (!numberOfColors) {
            if (same9[a] == 0) {
                same9[a] = 1;
            } else {
                same9[a] = 0;
            }
            if (same9[b] == 0) {
                same9[b] = 1;
            } else {
                same9[b] = 0;
            }
            if (same9[c] == 0) {
                same9[c] = 1;
            } else {
                same9[c] = 0;
            }
            if (same9[d] == 0) {
                same9[d] = 1;
            } else {
                same9[d] = 0;
            }
            if (same9[e] == 0) {
                same9[e] = 1;
            } else {
                same9[e] = 0;
            }
        } else {
            if (same9[a] == 0) {
                same9[a] = 1;
            } else if (same9[a] == 1) {
                same9[a] = 20;
            } else {
                same9[a] = 0;
            }
            if (same9[b] == 0) {
                same9[b] = 1;
            } else if (same9[b] == 1) {
                same9[b] = 20;
            } else {
                same9[b] = 0;
            }
            if (same9[c] == 0) {
                same9[c] = 1;
            } else if (same9[c] == 1) {
                same9[c] = 20;
            } else {
                same9[c] = 0;
            }
            if (same9[d] == 0) {
                same9[d] = 1;
            } else if (same9[d] == 1) {
                same9[d] = 20;
            } else {
                same9[d] = 0;
            }
            if (same9[e] == 0) {
                same9[e] = 1;
            } else if (same9[e] == 1) {
                same9[e] = 20;
            } else {
                same9[e] = 0;
            }
        }

    }

    @SuppressLint("SetTextI18n")
    private void checkWin9() {
        int oldAverage = 0;
        int win = 0;
        for (int i = 0; i < 9; i++) {
            win = win + same9[i];
        }
        if (!numberOfColors && (win == 9 || win == 0)) {
            numberGames2++;
            numberCounter2 += counter;
            oldAverage = average2;
            average2 = numberCounter2 / numberGames2;
            oldAverage = average2 - oldAverage;
            String StrOldAverage = "";
            if (oldAverage > 0) {
                StrOldAverage = "+" + oldAverage;
            } else {
                StrOldAverage = Integer.toString(oldAverage);
            }
            StrOldAverage2 = StrOldAverage;
            if (bestAttempt2 == 0) {
                bestAttempt2 = counter;
            }
            if (bestAttempt2 > counter) {
                bestAttempt2 = counter;
            }
            if (worstAttempt2 == 0) {
                worstAttempt2 = counter;
            }
            if (worstAttempt2 < counter) {
                worstAttempt2 = counter;
            }
            txtBestAttempt.setText("best attempt: " + bestAttempt2
                    + "\naverage: " + average2 + " (" + StrOldAverage + ")"
                    + "\nworst attempt: " + worstAttempt2);
            showSnackebar();
        }
        if (numberOfColors && (win == 9 || win == 0 || win == 180)) {
            numberGames3++;
            numberCounter3 += counter;
            oldAverage = average3;
            average3 = numberCounter3 / numberGames3;
            oldAverage = average3 - oldAverage;
            String strOldAverage = "";
            if (oldAverage > 0) {
                strOldAverage = "+" + oldAverage;
            } else {
                strOldAverage = Integer.toString(oldAverage);
            }
            StrOldAverage3 = strOldAverage;
            if (bestAttempt3 == 0) {
                bestAttempt3 = counter;
            }
            if (bestAttempt3 > counter) {
                bestAttempt3 = counter;
            }
            if (worstAttempt3 == 0) {
                worstAttempt3 = counter;
            }
            if (worstAttempt3 < counter) {
                worstAttempt3 = counter;
            }
            txtBestAttempt.setText("best attempt: " + bestAttempt3
                    + "\naverage: " + average3 + " (" + strOldAverage + ")"
                    + "\nworst attempt: " + worstAttempt3);
            showSnackebar();
        }
    }

    private void loadCorrectColor() {
        if (same9[0] == 0) {
            btnA1.setBackgroundColor(Color.parseColor(color1));
        } else if (same9[0] == 1) {
            btnA1.setBackgroundColor(Color.parseColor(color2));
        } else {
            btnA1.setBackgroundColor(Color.parseColor(color3));
        }

        if (same9[1] == 0) {
            btnA2.setBackgroundColor(Color.parseColor(color1));
        } else if (same9[1] == 1) {
            btnA2.setBackgroundColor(Color.parseColor(color2));
        } else {
            btnA2.setBackgroundColor(Color.parseColor(color3));
        }

        if (same9[2] == 0) {
            btnA3.setBackgroundColor(Color.parseColor(color1));
        } else if (same9[2] == 1) {
            btnA3.setBackgroundColor(Color.parseColor(color2));
        } else {
            btnA3.setBackgroundColor(Color.parseColor(color3));
        }

        if (same9[3] == 0) {
            btnB1.setBackgroundColor(Color.parseColor(color1));
        } else if (same9[3] == 1) {
            btnB1.setBackgroundColor(Color.parseColor(color2));
        } else {
            btnB1.setBackgroundColor(Color.parseColor(color3));
        }

        if (same9[4] == 0) {
            btnB2.setBackgroundColor(Color.parseColor(color1));
        } else if (same9[4] == 1) {
            btnB2.setBackgroundColor(Color.parseColor(color2));
        } else {
            btnB2.setBackgroundColor(Color.parseColor(color3));
        }

        if (same9[5] == 0) {
            btnB3.setBackgroundColor(Color.parseColor(color1));
        } else if (same9[5] == 1) {
            btnB3.setBackgroundColor(Color.parseColor(color2));
        } else {
            btnB3.setBackgroundColor(Color.parseColor(color3));
        }

        if (same9[6] == 0) {
            btnC1.setBackgroundColor(Color.parseColor(color1));
        } else if (same9[6] == 1) {
            btnC1.setBackgroundColor(Color.parseColor(color2));
        } else {
            btnC1.setBackgroundColor(Color.parseColor(color3));
        }

        if (same9[7] == 0) {
            btnC2.setBackgroundColor(Color.parseColor(color1));
        } else if (same9[7] == 1) {
            btnC2.setBackgroundColor(Color.parseColor(color2));
        } else {
            btnC2.setBackgroundColor(Color.parseColor(color3));
        }

        if (same9[8] == 0) {
            btnC3.setBackgroundColor(Color.parseColor(color1));
        } else if (same9[8] == 1) {
            btnC3.setBackgroundColor(Color.parseColor(color2));
        } else {
            btnC3.setBackgroundColor(Color.parseColor(color3));
        }

    }

    private void randomizeGame() {
        counter = 0;
        runGame = true;
        Random random = new Random();
        if (!numberOfColors) {
            do {
                for (int i = 0; i < 9; i++) {
                    same9[i] = random.nextInt(2);
                }
            } while (checkComplexity(depthM));
        } else {
            do {
                for (int i = 0; i < 9; i++) {
                    same9[i] = random.nextInt(3);
                }
                for (int i = 0; i < 9; i++) {
                    if (same9[i] == 2) {
                        same9[i] = 20;
                    }
                }
            } while (checkWin(same9));
        }
        loadCorrectColor();
    }

    private boolean checkComplexity(int depth) {
        boolean complex = false;

        if (checkWin(same9)) {
            complex = true;
        }
        for (int i = 0; i < depth; i++) {
            switchA1(same9);
            if (checkWin(same9)) {
                switchA1(same9);
                complex = true;
                break;
            } else {
                complex = checkComplexity(depth - 1);
                switchA1(same9);
                if (complex) {
                    break;
                }
            }

            switchA2(same9);
            if (checkWin(same9)) {
                switchA2(same9);
                complex = true;
                break;
            } else {
                complex = checkComplexity(depth - 1);
                switchA2(same9);
                if (complex) {
                    break;
                }
            }

            switchA3(same9);
            if (checkWin(same9)) {
                switchA3(same9);
                complex = true;
                break;
            } else {
                complex = checkComplexity(depth - 1);
                switchA3(same9);
                if (complex) {
                    break;
                }
            }

            switchB1(same9);
            if (checkWin(same9)) {
                switchB1(same9);
                complex = true;
                break;
            } else {
                complex = checkComplexity(depth - 1);
                switchB1(same9);
                if (complex) {
                    break;
                }
            }

            switchB2(same9);
            if (checkWin(same9)) {
                switchB2(same9);
                complex = true;
                break;
            } else {
                complex = checkComplexity(depth - 1);
                switchB2(same9);
                if (complex) {
                    break;
                }
            }

            switchB3(same9);
            if (checkWin(same9)) {
                switchB3(same9);
                complex = true;
                break;
            } else {
                complex = checkComplexity(depth - 1);
                switchB3(same9);
                if (complex) {
                    break;
                }
            }

            switchC1(same9);
            if (checkWin(same9)) {
                switchC1(same9);
                complex = true;
                break;
            } else {
                complex = checkComplexity(depth - 1);
                switchC1(same9);
                if (complex) {
                    break;
                }
            }

            switchC2(same9);
            if (checkWin(same9)) {
                switchC2(same9);
                complex = true;
                break;
            } else {
                complex = checkComplexity(depth - 1);
                switchC2(same9);
                if (complex) {
                    break;
                }
            }

            switchC3(same9);
            if (checkWin(same9)) {
                switchC3(same9);
                complex = true;
                break;
            } else {
                complex = checkComplexity(depth - 1);
                switchC3(same9);
                if (complex) {
                    break;
                }
            }
        }
        return complex;
    }

    private static void switchA1(int[] same) {
        if (same[0] == 0) {
            same[0] = 1;
        } else {
            same[0] = 0;
        }
        if (same[1] == 0) {
            same[1] = 1;
        } else {
            same[1] = 0;
        }
        if (same[3] == 0) {
            same[3] = 1;
        } else {
            same[3] = 0;
        }
    }

    private static void switchA2(int[] same) {
        if (same[1] == 0) {
            same[1] = 1;
        } else {
            same[1] = 0;
        }
        if (same[0] == 0) {
            same[0] = 1;
        } else {
            same[0] = 0;
        }
        if (same[2] == 0) {
            same[2] = 1;
        } else {
            same[2] = 0;
        }
        if (same[4] == 0) {
            same[4] = 1;
        } else {
            same[4] = 0;
        }
    }

    private static void switchA3(int[] same) {
        if (same[2] == 0) {
            same[2] = 1;
        } else {
            same[2] = 0;
        }
        if (same[1] == 0) {
            same[1] = 1;
        } else {
            same[1] = 0;
        }
        if (same[5] == 0) {
            same[5] = 1;
        } else {
            same[5] = 0;
        }
    }

    private static void switchB1(int[] same) {
        if (same[3] == 0) {
            same[3] = 1;
        } else {
            same[3] = 0;
        }
        if (same[0] == 0) {
            same[0] = 1;
        } else {
            same[0] = 0;
        }
        if (same[6] == 0) {
            same[6] = 1;
        } else {
            same[6] = 0;
        }
        if (same[4] == 0) {
            same[4] = 1;
        } else {
            same[4] = 0;
        }
    }

    private static void switchB2(int[] same) {
        if (same[4] == 0) {
            same[4] = 1;
        } else {
            same[4] = 0;
        }
        if (same[1] == 0) {
            same[1] = 1;
        } else {
            same[1] = 0;
        }
        if (same[7] == 0) {
            same[7] = 1;
        } else {
            same[7] = 0;
        }
        if (same[3] == 0) {
            same[3] = 1;
        } else {
            same[3] = 0;
        }
        if (same[5] == 0) {
            same[5] = 1;
        } else {
            same[5] = 0;
        }
    }

    private static void switchB3(int[] same) {
        if (same[5] == 0) {
            same[5] = 1;
        } else {
            same[5] = 0;
        }
        if (same[4] == 0) {
            same[4] = 1;
        } else {
            same[4] = 0;
        }
        if (same[2] == 0) {
            same[2] = 1;
        } else {
            same[2] = 0;
        }
        if (same[8] == 0) {
            same[8] = 1;
        } else {
            same[8] = 0;
        }
    }

    private static void switchC1(int[] same) {
        if (same[6] == 0) {
            same[6] = 1;
        } else {
            same[6] = 0;
        }
        if (same[7] == 0) {
            same[7] = 1;
        } else {
            same[7] = 0;
        }
        if (same[3] == 0) {
            same[3] = 1;
        } else {
            same[3] = 0;
        }
    }

    private static void switchC2(int[] same) {
        if (same[7] == 0) {
            same[7] = 1;
        } else {
            same[7] = 0;
        }
        if (same[6] == 0) {
            same[6] = 1;
        } else {
            same[6] = 0;
        }
        if (same[8] == 0) {
            same[8] = 1;
        } else {
            same[8] = 0;
        }
        if (same[4] == 0) {
            same[4] = 1;
        } else {
            same[4] = 0;
        }
    }

    private static void switchC3(int[] same) {
        if (same[8] == 0) {
            same[8] = 1;
        } else {
            same[8] = 0;
        }
        if (same[7] == 0) {
            same[7] = 1;
        } else {
            same[7] = 0;
        }
        if (same[5] == 0) {
            same[5] = 1;
        } else {
            same[5] = 0;
        }
    }

    public static boolean checkWin(int[] same) {
        int win = 0;
        for (int j : same) {
            win += j;
        }
        return win == 0 || win == 9 || win == 180;
    }

    private void showSnackebar() {
        runGame = false;
        Snackbar snackbar = Snackbar.make(parent, "You won and took " + counter + " moves.", Snackbar.LENGTH_INDEFINITE)
                .setAction("keep playing", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        randomizeGame();
                    }
                }).setActionTextColor(Color.BLUE);
        View snackbarView = snackbar.getView();
        TextView tv = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setMaxLines(2);
        snackbar.show();
    }

    private void initStuff() {
        btnA1 = findViewById(R.id.btnA1);
        btnA1.setOnClickListener(this);
        btnA2 = findViewById(R.id.btnA2);
        btnA2.setOnClickListener(this);
        btnA3 = findViewById(R.id.btnA3);
        btnA3.setOnClickListener(this);
        btnB1 = findViewById(R.id.btnB1);
        btnB1.setOnClickListener(this);
        btnB2 = findViewById(R.id.btnB2);
        btnB2.setOnClickListener(this);
        btnB3 = findViewById(R.id.btnB3);
        btnB3.setOnClickListener(this);
        btnC1 = findViewById(R.id.btnC1);
        btnC1.setOnClickListener(this);
        btnC2 = findViewById(R.id.btnC2);
        btnC2.setOnClickListener(this);
        btnC3 = findViewById(R.id.btnC3);
        btnC3.setOnClickListener(this);

        imgNumberColors = findViewById(R.id.imgNumberColors);
        imgColors = findViewById(R.id.imgColors);

        color1 = "#b3b3ff";
        color2 = "#131339";
        color3 = "#4780b8";
    }
}